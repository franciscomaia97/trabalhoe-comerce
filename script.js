let User = function (fName, lName, email, password) {
    if (arguments.length != 4 ||
        typeof fName !== "string" ||
        typeof lName !== "string" ||
        typeof email !== "string" ||
        typeof password !== "string"
    )
        return;

    this.fName = fName;
    this.lName = lName;
    this.email = email;
    this.password = password;
}

User.prototype = {


    obtainName: function () {
        return this.fName + " " + this.lName;
    },
    verifyUser: function (email, password) {
        if (arguments.length !== 2 ||
            typeof email !== "string" ||
            typeof password !== "string")
            return false;

        if (email === this.email && password === this.password)
            return true;
        else
            return false;
    },
    verifyEmail: function (email) {
        if (arguments.length !== 1 ||
            typeof email !== "string")
            return false;

        if (email === this.email)
            return true;
        else
            return false;
    }
};

let users = [],
    leftButton = 0,
    rightButton = 0,
    fName = 0,
    lName = 0,
    email = 0,
    password = 0,
    registerData = 0,
    inputData = 0,
    formElements = 0,
    purposeDescription = 0,
    autenticated = -1;

function processLogin() {
    if (verifyEmail(email.value) && password.value.length !== 0)
        for (let i = 0; i < users.length; i++)
            if (users[i].verifyUser(email.value, password.value)) {
                autenticated = i;
            }

    if (autenticated !== -1) {
        //User is autenticated
        //1 - Update purposeDescription and clear all inputs
        purposeDescription.innerHTML = "Bem vindo " + " " + users[autenticated].obtainName() + "!";
        clearInput();

        //2 - Hide all form date
        hideAllFormElements();
        //3 - Repurpose buttons - Logout and Edit
        // remove previous listeners
        leftButton.removeEventListener("click", processLogin);
        rightButton.removeEventListener("click", processRegister);
        //2 - program new listeners
        leftButton.addEventListener("click", processLogout);
        rightButton.addEventListener("click", processEdit);
        //3 - Update buttons caption
        leftButton.value = "Logout";
        rightButton.value = "Edit";
    }
    else
        output.innerHTML = "Utilizador não reconhecido!";

}

function processLogout() {
    //update autenticated user to -1
    autenticated = -1;

    //update purpose description
    purposeDescription.innerHTML = "Login";

    //show autentication data
    showAllformElements();
    hideRegisterData();

    leftButton.removeEventListener("click", processLogout);
    rightButton.removeEventListener("click", processEdit);
    //2 - program new listeners
    leftButton.addEventListener("click", processLogin);
    rightButton.addEventListener("click", processRegister);
    //3 - Update buttons caption
    leftButton.value = "Login";
    rightButton.value = "Register";
}

function processEdit() {

}

function processRegisterNew() {
    //capturar e introduzir os dados do utilizador


    //Verificar se os dados introduzidos estão corretos

    //1 - Verificar conteúdo pName
    //2 - Verificar conteúdo uName
    //3 - Verificar se o email é válido
    //4 - check password (verificação mínima)
    if (fName.value.trim().length === 0  || lName.value.trim().length === 0 || !verifyEmail(email.value) || password.value.trim().length === 0) {

        output.innerHTML = "Os dados fornecidos não são válidos."
        return;
    }

    //Guardar dados no objecto
    //1- Validar se o email já existe
    //2- Se o email não existe, criar novo user
    //3 - adicionar utilizador ao array

    //Guardar dados no objecto
    //user.registar(pNome.value, pNome.value, email.value, pass.value)

    //1
    for (let i = 0; i < users.length; i++) {
        if (users[i].verifyEmail(email.value)) {
            output.innerHTML = "O email indicado já existe."
            return;
        }
    }

    //2
    let user = new User(fName.value, lName.value, email.value, password.value);

    //3
    users.push(user);

    //limpar os dados
    clearInput();
    //Informar o user de registo com sucesso 
    output.innerHTML = "Utilizador criado com sucesso."
}
function processRegister() {
    //update purposeDescripton
    purposeDescription.innerHTML = "Login";
    //Show all data
    clearInput();
    //Process buttons

    showRegisterData();

    //1 - remove previous listeners
    leftButton.removeEventListener("click", processLogin);
    rightButton.removeEventListener("click", processRegister);
    //2 - program new listeners
    leftButton.addEventListener("click", processRegisterNew);
    rightButton.addEventListener("click", processBack);

    leftButton.value = "Registar";
    rightButton.value = "Voltar;"
}

function processBack() {
    //update purposeDescripton
    purposeDescription.innerHTML = "Login";
    //Show all data
    clearInput();
    //Process buttons

    hideRegisterData();

    //1 - remove previous listeners
    leftButton.removeEventListener("click", processRegisterNew);
    rightButton.removeEventListener("click", processBack);
    //2 - program new listeners
    leftButton.addEventListener("click", processLogin);
    rightButton.addEventListener("click", processRegister);
    //3 - Update buttons caption
    leftButton.value = "Login";
    rightButton.value = "Register";
}

function clearInput() {
    for (let i = 0; i < inputData.length; i++)
        inputData[i].value = "";
}

function hideRegisterData() {
    for (let i = 0; i < registerData.length; i++)
        registerData[i].style.display = "none";
}

function showRegisterData() {
    for (let i = 0; i < registerData.length; i++)
        registerData[i].style.display = "block";
}

function hideAllFormElements() {
    for (let i = 0; i < formElements.length; i++)
        formElements[i].style.display = "none";
}

function showAllformElements() {
    for (let i = 0; i < formElements.length; i++)
        formElements[i].style.display = "block";
}

function verifyEmail(email) {
    let arrAux = [];

    if (arguments.length != 1 || typeof email !== "string")
        return false;

    arrAux = email.split("@");
    if (arrAux.length != 2)
        return false;

    if (arrAux[1].indexOf(".") === -1)
        return false;

    if (arrAux[0].length === 0)
        return false;

    return true;
}

function createDefault() {
    let user = new User("João", "Ratão", "jrato@gmail.com", "12345");

    users.push(user);
}

window.onload = function () {
    //get button handlers
    leftButton = document.getElementById("leftButton");
    rightButton = document.getElementById("rightButton");

    //get data input handlers
    fName = document.getElementById("fName");
    lName = document.getElementById("lName");
    email = document.getElementById("email");
    password = document.getElementById("password");

    //get handler output
    output = document.getElementById("output");

    registerData = document.getElementsByClassName("registerData");

    inputData = document.getElementsByClassName("inputData");

    formElements = document.getElementsByClassName("formElements");

    //get purposeDescription handler
    purposeDescription = document.getElementById("purposeDescription");

    hideRegisterData();

    leftButton.addEventListener("click", processLogin)
    rightButton.addEventListener("click", processRegister);

    createDefault();
}